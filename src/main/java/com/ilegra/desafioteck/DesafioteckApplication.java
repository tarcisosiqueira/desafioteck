package com.ilegra.desafioteck;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioteckApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(DesafioteckApplication.class, args);
		
		FileWatcher fileWatcher = new FileWatcher();
		fileWatcher.listener();
	}

}
