package com.ilegra.desafioteck;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "")
public class Desafio {

	@ApiOperation(value = "")
	public static void main() throws IOException {

		final String diretorioIn = "C:\\Users\\VISITAS\\data\\in\\";
		final String diretorioOut = "C:\\Users\\VISITAS\\data\\out\\";
		String filename = "";

		System.out.println("Iniciando geração de relatório:");
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(diretorioIn))) {
			for (Path path : stream) {
				filename = path.getFileName().toString();
			}
		}
		final Reader reader = Files.newBufferedReader(Paths.get(diretorioIn + filename));

		File file = new File(diretorioIn + filename);
		file.delete();
		System.out.printf("Arquivo %s coletado com sucesso\n", filename);

		final CSVParser parser = new CSVParserBuilder().withSeparator('ç').withIgnoreQuotations(true).build();
		final CSVReader csvReader = new CSVReaderBuilder(reader).withCSVParser(parser).build();

		List<String[]> linhas = csvReader.readAll();
		FileWriter arq = new FileWriter(diretorioOut + "RELATORIO.txt");
		PrintWriter gravarArq = new PrintWriter(arq);

		final String VENDEDOR = "001";
		final String CLIENTE = "002";
		final String VENDA = "003";

		boolean validacao = true;

		int contadorClientes = 0;
		int contadorVendedores = 0;
		int IDMaiorVenda = 0;
		Double somaVendas = 0D;
		String piorVendedor = "";

		for (String[] coluna : linhas) {

			try {
				switch (coluna[0]) {
				case VENDEDOR:
					contadorVendedores++;
					break;
				case CLIENTE:
					contadorClientes++;
					break;
				case VENDA:
					Double soma = (new Utils().calc(coluna[2]));

					// Verifica se a soma da venda é apior do arquivo e seta como pior vendedor
					// verifica também se a soma é zero, considerando assim a primeira venda.
					if ((soma < somaVendas) || (somaVendas == 0)) {
						piorVendedor = coluna[3];
					}

					// Verifica se a soma das vendas é a maior do arquivo e seta o ID da venda
					if (soma > somaVendas) {
						somaVendas = soma;
						IDMaiorVenda = Integer.parseInt(coluna[1]);
					} else {
						IDMaiorVenda = IDMaiorVenda;
					}

					break;

				default:
					// Em caso de não cair em nenhum cenario retorna um erro
					// Poderia se criar uma classe para validação do arquivo,
					// evitando erros neste switch
					throw new RuntimeException("Error");
				}

			} catch (Exception e) {
				// Valida se o arquivo esta com o minimo do layout correto
				// caso negativo não gera o relatório e não trava a aplicação
				System.out.println("Erro ao processar arquivo");
				validacao = false;
			}

		}

		if (validacao) {
			gravarArq.printf("Quantidade de clientes: %d \n", contadorClientes);
			gravarArq.printf("Quantidade de vendedores: %d \n", contadorVendedores);
			gravarArq.printf("ID da venda mais cara: %d \n", IDMaiorVenda);
			gravarArq.printf("O pior vendedor: %s \n", piorVendedor);

			System.out.printf("Relatório criado com sucesso no diretório: %s\n", diretorioOut);
		}
		arq.close();
	}

}
