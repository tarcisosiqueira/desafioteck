package com.ilegra.desafioteck;

import java.util.Arrays;
import java.util.Optional;

public class Utils {

    public Double calc(String param) {
        return Arrays.stream(
                Optional.ofNullable(param).orElse("")
                .replaceAll("\\[(.*)]", "$1").split(","))
                .filter(s -> s.matches("\\d+-\\d+-\\d+\\.\\d+"))
                .map(s -> s.replaceAll("\\d+-\\d+-(\\d+)", "$1"))
                .map(Double::valueOf)
                .reduce(Double::sum)
                .orElse(0D);
    }
}